package handlers

import (
	"MReview-Api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Images struct {
	ID       uint   `json:"id"`
	Image    string `json:"image"`
	Movie_ID uint   `json:"movie_id"`
}

type ImagesHandler struct {
	ims models.ImagesService
}

func NewImagesHandler(ims models.ImagesService) *ImagesHandler {
	return &ImagesHandler{ims}
}

type CreateNewImages struct {
	Image    string `json:"image"`
	Movie_ID uint   `json:"movie_id"`
}

//------------------------ Create Images -----------------------

func (imh *ImagesHandler) CreateImages(c *gin.Context) {
	movieIDStr := c.Param("id")
	movieID, err := strconv.Atoi(movieIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data := new(CreateNewImages)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	image := new(models.Images)
	image.Images = data.Image
	image.Movie_ID = uint(movieID)
	if err := imh.ims.CreateImages(image); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(Images)
	res.ID = image.ID
	res.Image = image.Images
	c.JSON(201, gin.H{
		"id":    image.ID,
		"image": image.Images,
	})
}

//----------------------- List Images ----------------------

func (imh *ImagesHandler) ListImages(c *gin.Context) {
	idSrting := c.Param("id") //ต้องเปลี่ยน id ให้ตรงกับ main.go
	ID, err := strconv.Atoi(idSrting)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := imh.ims.ListImages(uint(ID))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}

//--------------------List One Image ------------------------

func (imh *ImagesHandler) GetOneImage(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := imh.ims.GetOneImage(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}
