package handlers

import (
	"MReview-Api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Movies struct {
	ID               uint   `json:"id"`
	Title            string `json:"title"`
	ImagesPoster     string `json:"images_poster"`
	ImagesBg         string `json:"images_bg"`
	Motion_Picture   string `json:"motion_picture"`
	Description      string `json:"description"`
	Running_Time_Min string `json:"running_time_min"`
	Release_Data     string `json:"release_data"`
	Status           string `json:"status"`
}
type MoviesHandler struct {
	ms models.MoviesService
}

func NewMoviesHandler(ms models.MoviesService) *MoviesHandler {
	return &MoviesHandler{ms}
}

//------------------ Create Movies---------------------------

type CreateMovies struct {
	Title            string `json:"title"`
	ImagesPoster     string `json:"images_poster"`
	ImagesBg         string `json:"images_bg"`
	Motion_Picture   string `json:"motion_picture"`
	Description      string `json:"description"`
	Running_Time_Min string `json:"running_time_min"`
	Release_Data     string `json:"release_data"`
	Status           string `json:"status"`
}

func (mh *MoviesHandler) CreateMovie(c *gin.Context) {
	data := new(CreateMovies)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	movie := new(models.Movie)
	movie.Title = data.Title
	movie.ImagesBg = data.ImagesBg
	movie.ImagesPoster = data.ImagesPoster
	movie.Motion_Picture = data.Motion_Picture
	movie.Description = data.Description
	movie.Running_Time_Min = data.Running_Time_Min
	movie.Release_Data = data.Release_Data
	movie.Status = data.Status
	if err := mh.ms.CreateMovies(movie); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(Movies)
	res.ID = movie.ID
	res.Title = movie.Title
	res.ImagesBg = movie.ImagesBg
	res.ImagesPoster = movie.ImagesPoster
	res.Motion_Picture = movie.Motion_Picture
	res.Description = movie.Description
	res.Running_Time_Min = movie.Running_Time_Min
	res.Release_Data = movie.Release_Data
	res.Status = movie.Status
	c.JSON(201, gin.H{
		"id":               movie.ID,
		"Title":            movie.Title,
		"Poster":           movie.ImagesPoster,
		"ImagesBg":         movie.ImagesBg,
		"PG":               movie.Motion_Picture,
		"Description":      movie.Description,
		"Running_Time_Min": movie.Running_Time_Min,
		"Release_Data":     movie.Release_Data,
		"Status":           movie.Status,
	})
}

//----------------- List Movies ---------------------------

type NewMovie struct {
	ID               uint   `json:"id"`
	Title            string `json:"title"`
	ImagesPoster     string `json:"images_poster"`
	ImagesBg         string `json:"images_bg"`
	Motion_Picture   string `json:"motion_picture"`
	Description      string `json:"description"`
	Running_Time_Min string `json:"running_time_min"`
	Release_Data     string `json:"release_data"`
	Status           string `json:"status"`
}

func (mh *MoviesHandler) ListMovie(c *gin.Context) {
	movies, err := mh.ms.ListMovie()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	newMovie := []NewMovie{}
	for _, d := range movies {
		newMovie = append(newMovie, NewMovie{
			ID:               d.ID,
			Title:            d.Title,
			ImagesPoster:     d.ImagesPoster,
			ImagesBg:         d.ImagesBg,
			Motion_Picture:   d.Motion_Picture,
			Description:      d.Description,
			Running_Time_Min: d.Running_Time_Min,
			Release_Data:     d.Release_Data,
			Status:           d.Status,
		})
	}
	c.JSON(200, newMovie)
}

//----------------------------- Get One -----------------------

func (mh *MoviesHandler) GetOne(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := mh.ms.GetMovieByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)

}

//----------------- Movie Show Genres --------------------------

func (mh *MoviesHandler) ListMovieGenres(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	list, err := mh.ms.GetMovieGenres(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, list)
}
