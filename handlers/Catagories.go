package handlers

import (
	"MReview-Api/models"

	"github.com/gin-gonic/gin"
)

type Catagories struct {
	ID              uint   `json:"id"`
	Catagories_Type string `json:"catagories_type"`
}

type CatagoriesHandler struct {
	cs models.CatagoriesService
}

func NewCatagoriesHandler(cs models.CatagoriesService) *CatagoriesHandler {
	return &CatagoriesHandler{cs}
}

type CreateNewCatagories struct {
	Catagories_Type string `json:"catagories_type"`
}

//--------------------Create Catagories ----------------------------

func (ch *CatagoriesHandler) CreateCatagories(c *gin.Context) {
	data := new(CreateNewCatagories)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	catagory := new(models.Catagories)
	catagory.Catagories_Type = data.Catagories_Type
	if err := ch.cs.CraterCatagories(catagory); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(Catagories)
	res.ID = catagory.ID
	res.Catagories_Type = catagory.Catagories_Type
	c.JSON(201, gin.H{
		"id":   catagory.ID,
		"Type": catagory.Catagories_Type,
	})
}

//-------------------- Catagories List --------------------

func (ch *CatagoriesHandler) ListTypeCatagories(c *gin.Context) {
	catagories, err := ch.cs.ListTypeCatagories()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	newCatagories := []Catagories{}
	for _, d := range catagories {
		newCatagories = append(newCatagories, Catagories{
			ID:              d.ID,
			Catagories_Type: d.Catagories_Type,
		})
	}
	c.JSON(200, newCatagories)

}
