package handlers

import (
	"MReview-Api/models"

	"github.com/gin-gonic/gin"
)

type ShowGenres struct {
	ID             uint `json:"id"`
	Movies_ID      uint `json:"movie_id"`
	MovieGenres_ID uint `json:"movie_genres_id"`
}

type ShowGenresHandler struct {
	shs models.ShowGenresService
}

type CreateShowGenres struct {
	Movies_ID      uint `json:"movie_id"`
	MovieGenres_ID uint `json:"movie_genres_id"`
}

func NewShowGenresHandler(shs models.ShowGenresService) *ShowGenresHandler {
	return &ShowGenresHandler{shs}
}

//-------------------------- Create Show Genres -------------------------
func (sgh *ShowGenresHandler) CreateMovieShowGenres(c *gin.Context) {
	data := new(CreateShowGenres)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	showgenres := new(models.ShowGenres)
	showgenres.Movie_ID = data.Movies_ID
	showgenres.MovieGenres_ID = data.MovieGenres_ID
	if err := sgh.shs.Create(showgenres); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(ShowGenres)
	res.ID = showgenres.ID
	res.Movies_ID = showgenres.Movie_ID
	res.MovieGenres_ID = showgenres.MovieGenres_ID
	c.JSON(201, showgenres)

}
