package handlers

import (
	"MReview-Api/models"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	us models.UserService
}

type ResgisterReq struct {
	UserName string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func NewUserHandler(us models.UserService) *UserHandler {
	return &UserHandler{us}
}

//----------------------Register -----------------------------

func (uh *UserHandler) Register(c *gin.Context) {
	req := new(ResgisterReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	user := new(models.User)
	user.UserName = req.UserName
	user.Email = req.Email
	user.Password = req.Password

	if err := uh.us.Create(user); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token":    user.Token,
		"email":    user.Email,
		"username": user.UserName,
	})
}

//-----------------------Login ----------------------------

type LoginReq struct {
	UserName string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (uh *UserHandler) Login(c *gin.Context) {
	req := new(LoginReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"meesage": err.Error(),
		})
		return
	}
	user := new(models.User)
	user.UserName = req.UserName
	user.Email = req.Email
	user.Password = req.Password
	token, err := uh.us.Login(user)
	if err != nil {
		c.JSON(401, gin.H{
			"meesage": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token":       token,
		"email":       user.Email,
		user.UserName: user.UserName,
	})
}
