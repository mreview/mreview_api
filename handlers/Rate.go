package handlers

import (
	"MReview-Api/models"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Rate struct {
	ID         uint   `json:"id"`
	Rate_Type  string `json:"rate_type"`
	Rate_Score int    `json:"rate_score"`
}

type RateHandler struct {
	rs models.RateService
}

func NewRateHandler(rs models.RateService) *RateHandler {
	return &RateHandler{rs}
}

// --------------------- Create Rate ------------------------

type CreateNewRate struct {
	Rate_Type  string `json:"rate_type"`
	Rate_Score int    `json:"rate_score"`
}

//------------------ Create Rate ------------------

func (rg *RateHandler) CreateRate(c *gin.Context) {

	reviewIDstr := c.Param("id")
	reviewID, err := strconv.Atoi(reviewIDstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	data := new(CreateNewRate)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	rate := new(models.Rate)
	rate.Rate_Type = data.Rate_Type
	rate.Rate_Score = data.Rate_Score
	rate.ReviewMovieID = uint(reviewID)
	fmt.Println("rate9999: ", rate)
	if err := rg.rs.CreateRate(rate); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Println("rate lasttt: ", rate)
	res := new(Rate)
	res.ID = rate.ID
	res.Rate_Type = rate.Rate_Type
	res.Rate_Score = rate.Rate_Score
	c.JSON(201, gin.H{
		"id":         res.ID,
		"rate_type":  res.Rate_Type,
		"rate_score": res.Rate_Score,
	})

}

//--------------------------- List Rate ---------------------------------------

func (rg *RateHandler) ListRate(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := rg.rs.GetRateByReviewID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)

}

//------------------------------- List Rate Movie -------------------------
func (rg *RateHandler) ListRateMovie(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := rg.rs.GetRateAllMovie(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)

}
