package handlers

import (
	"MReview-Api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type AllCatagories struct {
	ID            uint `json:"id"`
	Movie_ID      uint `json:"movie_id"`
	Catagories_ID uint `json:"catagories_id"`
}

type AllCatagoriesHandlers struct {
	acs models.AllCatagoriesService
}

func NewAllCatagoriesHandlers(acs models.AllCatagoriesService) *AllCatagoriesHandlers {
	return &AllCatagoriesHandlers{acs}
}

type CreateNewAllCatagories struct {
	Movie_ID      uint `json:"movie_id"`
	Catagories_ID uint `json:"catagories_id"`
}

//------------------------ Create All Catagories------------------------

func (ach *AllCatagoriesHandlers) CreateAllCatagories(c *gin.Context) {
	data := new(CreateNewAllCatagories)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	allcatagories := new(models.AllCatagories)
	allcatagories.Movie_ID = data.Movie_ID
	allcatagories.Catagories_ID = data.Catagories_ID
	if err := ach.acs.Create(allcatagories); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(AllCatagories)
	res.Movie_ID = allcatagories.Movie_ID
	res.Catagories_ID = allcatagories.Catagories_ID
	c.JSON(201, allcatagories)
}

//----------------------- List Movie Catagories ------------------

func (ach *AllCatagoriesHandlers) ListCatagories(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	list, err := ach.acs.List(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, list)

}
