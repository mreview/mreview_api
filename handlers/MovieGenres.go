package handlers

import (
	"MReview-Api/models"

	"github.com/gin-gonic/gin"
)

type MovieGenres struct {
	Gen_ID   uint   `json:"gen_id"`
	Gen_Type string `json:"gen_type"`
}

type MovieGenresHandler struct {
	mgs models.MovieGenresService
}

func NewMovieGenresHandler(mgs models.MovieGenresService) *MovieGenresHandler {
	return &MovieGenresHandler{mgs}
}

type CreateNewMovieGenres struct {
	Gen_Type string `json:"gen_type"`
}

//----------------------- Create Movie Ganres -------------------

func (mgh *MovieGenresHandler) CreateMovieGenres(c *gin.Context) {
	data := new(CreateNewMovieGenres)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	moviegenres := new(models.MovieGenres)
	moviegenres.Gen_Type = data.Gen_Type
	if err := mgh.mgs.CreateMovieGenres(moviegenres); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(MovieGenres)
	res.Gen_ID = moviegenres.ID
	res.Gen_Type = moviegenres.Gen_Type
	c.JSON(201, gin.H{
		"gen_id":   moviegenres.ID,
		"gen_type": moviegenres.Gen_Type,
	})
}

//---------------------- List GenresType -------------------------------

type NewGenres struct {
	ID       uint   `json:"id"`
	Gen_Type string `json:"gen_type"`
}

func (mgh *MovieGenresHandler) ListGenres(c *gin.Context) {
	genres, err := mgh.mgs.ListGenres()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	newgenres := []NewGenres{}
	for _, d := range genres {
		newgenres = append(newgenres, NewGenres{
			ID:       d.ID,
			Gen_Type: d.Gen_Type,
		})
	}
	c.JSON(200, newgenres)

}
