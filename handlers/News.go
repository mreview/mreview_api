package handlers

import (
	"MReview-Api/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type News struct {
	ID         uint   `json:"id"`
	HeaderNews string `json:"header_news"`
	ShotDetail string `json:"shot_detail"`
	Date       string `json:"date"`
	Detail     string `json:"detail"`
	Image      string `json:"image"`
	CreateBy   string `json:"create_by"`
	Link       string `json:"link"`
}

type NewsHandler struct {
	ns models.NewsService
}

func NewNewsHandler(ns models.NewsService) *NewsHandler {
	return &NewsHandler{ns}
}

//----------------- Create News ----------------------

type CreateNews struct {
	gorm.Model
	HeaderNews string `json:"header_news"`
	ShotDetail string `json:"shot_detail"`
	Detail     string `json:"detail"`
	Date       string `json:"date"`
	Image      string `json:"image"`
	CreateBy   string `json:"create_by"`
	Link       string `json:"link"`
}

func (nh *NewsHandler) CreateNews(c *gin.Context) {
	data := new(CreateNews)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	news := new(models.News)
	news.HeaderNews = data.HeaderNews
	news.ShotDetail = data.ShotDetail
	news.Detail = data.Detail
	news.Date = data.Date
	news.Image = data.Image
	news.CreateBy = data.CreateBy
	news.Link = data.Link
	if err := nh.ns.CreateNews(news); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(News)
	res.ID = news.ID
	res.HeaderNews = news.HeaderNews
	res.ShotDetail = news.ShotDetail
	res.Detail = news.Detail
	res.Date = news.Date
	res.Image = news.Image
	res.CreateBy = news.CreateBy
	res.Link = news.Link
	c.JSON(201, gin.H{
		"ID":         news.ID,
		"HeaderNews": news.HeaderNews,
		"Detail":     news.Detail,
		"CreateBy":   news.CreateBy,
		"Link":       news.Link,
		"Image":      news.Image,
	})

}

//------------------- List News ---------------------------

type NewNews struct {
	ID         uint   `json:"id"`
	HeaderNews string `json:"header_news"`
	ShotDetail string `json:"shot_detail"`
	Date       string `json:"date"`
	Detail     string `json:"detail"`
	Image      string `json:"image"`
	CreateBy   string `json:"create_by"`
	Link       string `json:"link"`
}

func (nh *NewsHandler) ListNews(c *gin.Context) {
	news, err := nh.ns.ListNews()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	newNews := []NewNews{}
	for _, d := range news {
		newNews = append(newNews, NewNews{
			ID:         d.ID,
			HeaderNews: d.HeaderNews,
			ShotDetail: d.ShotDetail,
			Date:       d.Date,
			Detail:     d.Detail,
			Image:      d.Image,
			CreateBy:   d.CreateBy,
			Link:       d.Link,
		})
	}
	c.JSON(200, newNews)
}
