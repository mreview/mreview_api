package handlers

import (
	"MReview-Api/context"
	"MReview-Api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ReviewMovie struct {
	ID            uint   `json:"id"`
	Review_Header string `json:"review_header"`
	Review_Text   string `json:"review_text" sql:"longtext"`
}

type ReviewMovieHandler struct {
	rs models.ReviewMovieService
}

func NewReviewMovieHandler(rs models.ReviewMovieService) *ReviewMovieHandler {
	return &ReviewMovieHandler{rs}
}

type CreateReview struct {
	Review_Header string `json:"review_header"`
	Review_Text   string `json:"review_text" sql:"longtext"`
}

//--------------------------- Create Review ---------------------------------

func (rh *ReviewMovieHandler) CreateReviewMovie(c *gin.Context) {
	user := context.User(c)
	if user == nil {
		c.Status(401)
		return
	}
	movieIDStr := c.Param("id")
	movieID, err := strconv.Atoi(movieIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	data := new(CreateReview)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	review := new(models.ReviewMovie)
	review.Review_Header = data.Review_Header
	review.Review_Text = data.Review_Text
	review.UserID = user.ID
	review.MovieID = uint(movieID)
	if err := rh.rs.CreateReviewMovie(review); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := new(ReviewMovie)
	res.ID = review.ID
	res.Review_Header = review.Review_Header
	res.Review_Text = review.Review_Text
	c.JSON(201, gin.H{
		"id":            review.ID,
		"review_deader": review.Review_Header,
		"review_text":   review.Review_Text,
	})

}

//-------------------------------List Movie Review ---------------------------------

func (rh *ReviewMovieHandler) ListReview(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := rh.rs.GetReviewByMovieID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, data)
}

//------------------------ Have User Review -----------------------------------

func (rh *ReviewMovieHandler) HaveUserReview(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	data, err := rh.rs.GetHaveUser(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(200, data)
}

//-------------------------- Get One Review By ID ---------------
