package main

import (
	"MReview-Api/config"
	"MReview-Api/handlers"
	"MReview-Api/hash"
	"MReview-Api/middleW"
	"MReview-Api/models"
	"log"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var db *gorm.DB

const hmacKey = "secret"

func main() {

	conf := config.Load()

	db, err := gorm.Open(
		"mysql",
		conf.Connection,
	)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	db.LogMode(true)

	if err := db.AutoMigrate( //Create table สามารถ เพิ่มขึ้นมาได้เลย ถ้าสร้าง struct ใน models
		&models.News{},
		&models.User{},
		&models.Movie{},
		&models.Catagories{},
		&models.MovieGenres{},
		&models.Rate{},
		&models.ReviewMovie{},
		&models.Images{},
		&models.ShowGenres{},
		&models.AllCatagories{},
	).Error; err != nil {
		log.Fatal(err)
	}

	hmac := hash.NewHMAC(conf.HMACKey) // conf

	acs := models.NewAllCatagoriesService(db)
	ach := handlers.NewAllCatagoriesHandlers(acs)

	ns := models.NewNewsService(db)
	nh := handlers.NewNewsHandler(ns)

	us := models.NewUserService(db, hmac)
	uh := handlers.NewUserHandler(us)

	ms := models.NewMoviesService(db)
	mh := handlers.NewMoviesHandler(ms)

	mgs := models.NewMovieGenresService(db)
	mgh := handlers.NewMovieGenresHandler(mgs)

	sgs := models.NewShowGenresService(db)
	sgh := handlers.NewShowGenresHandler(sgs)

	rms := models.NewReviewMovieService(db)
	rmh := handlers.NewReviewMovieHandler(rms)

	cs := models.NewCatagoriesService(db)
	ch := handlers.NewCatagoriesHandler(cs)

	ims := models.NewImageService(db)
	imh := handlers.NewImagesHandler(ims)

	rs := models.NewRateService(db)
	rh := handlers.NewRateHandler(rs)

	r := gin.Default()

	r.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "PUT", "PATCH", "POST", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}

	r.POST("/news", nh.CreateNews)
	r.GET("/news", nh.ListNews)

	r.POST("/movie", mh.CreateMovie)
	r.GET("/movie", mh.ListMovie)
	r.GET("/movie/:id", mh.GetOne)

	r.POST("/movie/:id/images", imh.CreateImages)
	r.GET("/movie/:id/images", imh.ListImages)
	r.GET("/image/:id", imh.GetOneImage)

	r.POST("/movie_gen", mgh.CreateMovieGenres)
	r.GET("/ganres", mgh.ListGenres)

	r.POST("/movie_gen_show", sgh.CreateMovieShowGenres)

	r.POST("/all_catagories", ach.CreateAllCatagories)

	r.GET("/catagories/:id/movie", ach.ListCatagories)

	r.POST("/catagories", ch.CreateCatagories)
	r.GET("/catagories", ch.ListTypeCatagories)

	r.GET("/movie/:id/detail", mh.ListMovieGenres)
	r.GET("/movie/:id/review", rmh.ListReview)
	r.GET("/movie/:id/userreview", rmh.HaveUserReview)

	r.GET("reviewmovie/:id/rate", rh.ListRate)
	r.GET("movie/:id/rate", rh.ListRateMovie)

	r.POST("/register", uh.Register)
	r.POST("/login", uh.Login)

	auth := r.Group("/auth")

	auth.Use(middleW.RequireUser(us))
	{
		auth.POST("/reviewmovie/:id", rmh.CreateReviewMovie)
		auth.POST("/reviewmovie/:id/rate", rh.CreateRate)

	}

	r.Run()

}
