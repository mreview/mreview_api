package models

import "github.com/jinzhu/gorm"

type ShowGenres struct {
	gorm.Model
	Movie_ID       uint `gorm:"column:movie_id"`
	MovieGenres_ID uint `gorm:"column:movie_genres_id"`
}

type ShowGenresService interface {
	Create(showGenres *ShowGenres) error
}

type showgenresGorm struct {
	db *gorm.DB
}

func NewShowGenresService(db *gorm.DB) ShowGenresService {
	return &showgenresGorm{db}
}

//------------------- Create Show Genres ------------------------

func (shg *showgenresGorm) Create(showGenres *ShowGenres) error {
	return shg.db.Create(showGenres).Error
}
