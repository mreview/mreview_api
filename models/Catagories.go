package models

import "github.com/jinzhu/gorm"

type Catagories struct {
	gorm.Model
	Catagories_Type string `gorm:"column:catagories_type"`
}

type CatagoriesService interface {
	CraterCatagories(catagories *Catagories) error
	ListTypeCatagories() ([]Catagories, error)
}

type catagoriesGorm struct {
	db *gorm.DB
}

func NewCatagoriesService(db *gorm.DB) CatagoriesService {
	return &catagoriesGorm{db}
}

//-------------------------- Create Catogories -------------------------

func (cg *catagoriesGorm) CraterCatagories(catagories *Catagories) error {
	return cg.db.Create(catagories).Error
}

//------------------ List Catagories -------------------------------------

func (cg *catagoriesGorm) ListTypeCatagories() ([]Catagories, error) {
	catagories := []Catagories{}
	err := cg.db.Find(&catagories).Error
	if err != nil {
		return nil, err
	}
	return catagories, nil
}
