package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type Rate struct {
	gorm.Model
	Rate_Type     string `gorm:"column:rate_type"`
	Rate_Score    int    `gorm:"column:rate_score"`
	ReviewMovieID uint   `gorm:"column:review_movie_id"`
}

type RateService interface {
	CreateRate(rate *Rate) error
	GetRateByReviewID(id uint) ([]Rate, error)
	GetRateAllMovie(id uint) ([]RateAllMovie, error)
}

type rateGorm struct {
	db *gorm.DB
}

func NewRateService(db *gorm.DB) RateService {
	return &rateGorm{db}
}

//---------------------------- Create Rate- --------------------

func (rg *rateGorm) CreateRate(rate *Rate) error {
	return rg.db.Create(rate).Error
}

//---------------------------List Rate -------------------

func (rg *rateGorm) GetRateByReviewID(id uint) ([]Rate, error) {
	rate := []Rate{}
	if err := rg.db.Where("review_movie_id =?", id).Find(&rate).Error; err != nil {
		return nil, err
	}
	return rate, nil
}

//---------------------------- List Rate All Movie-------------------------

type RateAllMovie struct {
	Score     int    `gorm:"column:score" json:"score"`
	Rate_Type string `gorm:"column:rate_type" json:"rate_type"`
	Movie_ID  uint   `gorm:"column:movie_id" json:"movie_id"`
	Review_ID uint   `gorm:"column:review_id" json:"review_id"`
}

func (rg *rateGorm) GetRateAllMovie(id uint) ([]RateAllMovie, error) {
	listRate := []RateAllMovie{}
	rows, err := rg.db.Table("rates").Select("rates.rate_score , rates.rate_type  , review_movies.id , review_movies.id").
		Joins("JOIN review_movies ON review_movies.id = rates.review_movie_id").
		Where("review_movies.movie_id=?", id).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		res := RateAllMovie{}
		err := rows.Scan(&res.Score, &res.Rate_Type, &res.Movie_ID, &res.Review_ID)
		if err != nil {
			fmt.Println(err)
		}
		listRate = append(listRate, res)
	}
	if err != nil {
		return nil, err
	}
	return listRate, nil
}
