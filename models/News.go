package models

import "github.com/jinzhu/gorm"

type News struct {
	gorm.Model
	HeaderNews string
	ShotDetail string
	Date       string
	Detail     string
	Image      string
	CreateBy   string
	Link       string
}

type NewsService interface {
	CreateNews(news *News) error
	ListNews() ([]News, error)
}

func NewNewsService(db *gorm.DB) NewsService {
	return &newsGorm{db}
}

type newsGorm struct {
	db *gorm.DB
}

//----------------------Create News----------------------------

func (ng *newsGorm) CreateNews(news *News) error {
	return ng.db.Create(news).Error
}

//-----------------List News --------------------------------------

func (ng *newsGorm) ListNews() ([]News, error) {
	news := []News{}
	err := ng.db.Find(&news).Error
	if err != nil {
		return nil, err
	}
	return news, nil
}
