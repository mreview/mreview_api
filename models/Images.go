package models

import (
	"github.com/jinzhu/gorm"
)

type Images struct {
	gorm.Model
	Images   string `json:"images" gorm:"column:images"`
	Movie_ID uint   `gorm:"not null" json:"movie_id"`
}

type ImagesService interface {
	CreateImages(images *Images) error
	ListImages(id uint) ([]Images, error)
	GetOneImage(id uint) (*Images, error)
}

type imagesGorm struct {
	db *gorm.DB
}

func NewImageService(db *gorm.DB) ImagesService {
	return &imagesGorm{db}
}

//----------------------- Create Images-----------------------

func (ims *imagesGorm) CreateImages(images *Images) error {
	return ims.db.Create(images).Error
}

//---------------------- List Images ----------------------

func (ims *imagesGorm) ListImages(id uint) ([]Images, error) {
	images := []Images{}
	err := ims.db.Where("movie_id=?", id).Find(&images).Error
	if err != nil {
		return nil, err
	}
	return images, nil
}

//---------------------- Get By ID ---------------------------

func (ims *imagesGorm) GetOneImage(id uint) (*Images, error) {
	image := new(Images)
	if err := ims.db.Find(image, id).Error; err != nil {
		return nil, err
	}
	return image, nil
}
