package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type Movie struct {
	gorm.Model
	Title            string `gorm:"column:title"`
	ImagesPoster     string `gorm:"column:images_poster"`
	ImagesBg         string `gorm:"column:images_bg"`
	Motion_Picture   string `gorm:"column:motion_picture"`
	Description      string `gorm:"column:description"`
	Running_Time_Min string `gorm:"column:running_time_min"`
	Release_Data     string `gorm:"column:release_data"`
	Status           string `gorm:"column:Status"`
}

type MoviesService interface {
	CreateMovies(movie *Movie) error
	ListMovie() ([]Movie, error)
	GetMovieByID(id uint) (*Movie, error)
	GetMovieGenres(id uint) ([]MovieDetailType, error)
}

type moviesGorm struct {
	db *gorm.DB
}

func NewMoviesService(db *gorm.DB) MoviesService {
	return &moviesGorm{db}

}

//----------------- Create Movies ----------------------

func (mg *moviesGorm) CreateMovies(movie *Movie) error {
	return mg.db.Create(movie).Error
}

//------------------List All Movies-------------------------

func (mg *moviesGorm) ListMovie() ([]Movie, error) {
	movie := []Movie{}
	err := mg.db.Find(&movie).Error
	if err != nil {
		return nil, err
	}
	return movie, nil
}

//------------------- Get By ID --------------------------
func (mg *moviesGorm) GetMovieByID(id uint) (*Movie, error) {
	movie := new(Movie)
	if err := mg.db.First(movie, id).Error; err != nil {
		return nil, err
	}
	return movie, nil
}

//--------------------- Get Movie Genres ------------------------
type MovieDetailType struct {
	Type string
}

func (mg *moviesGorm) GetMovieGenres(id uint) ([]MovieDetailType, error) {
	listType := []MovieDetailType{}
	rows, err := mg.db.Table("show_genres").Select("movie_genres.gen_type").
		Joins("JOIN movie_genres ON movie_genres.id = show_genres.movie_genres_id").
		Where("show_genres.movie_id=?", id).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		res := MovieDetailType{}
		err := rows.Scan(&res.Type)
		if err != nil {
			fmt.Println(err)
		}
		listType = append(listType, res)
	}
	if err != nil {
		return nil, err
	}
	return listType, nil
}
