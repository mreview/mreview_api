package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type AllCatagories struct {
	Movie_ID      uint `gorm:"column:movie_id"`
	Catagories_ID uint `gorm:"column:catagories_id"`
}

type AllCatagoriesService interface {
	Create(allcatagories *AllCatagories) error
	List(id uint) ([]NewGetCatagories, error)
}

type allcatagoriesGorm struct {
	db *gorm.DB
}

func NewAllCatagoriesService(db *gorm.DB) AllCatagoriesService {
	return &allcatagoriesGorm{db}
}

//------------------- Create All Catagories --------------------------

func (acs *allcatagoriesGorm) Create(allcatagories *AllCatagories) error {
	return acs.db.Create(allcatagories).Error

}

//---------------------- Get List Catagories ------------------------------

type NewGetCatagories struct {
	ID          uint   `gorm:"column:id" json:"id"`
	Title       string `gorm:"column:titel" json:"title"`
	Poster      string `gorm:"column:poster" json:"poster"`
	Date        string `gorm:"column:date" json:"date"`
	Description string `gorm:"column:description" json:"description"`
	ImgBg       string `gorm:"column:imgBg" json:"imgBg"`
}

func (acs *allcatagoriesGorm) List(id uint) ([]NewGetCatagories, error) {
	listCatagories := []NewGetCatagories{}
	rows, err := acs.db.Table("movies").Select("movies.id,movies.title ,movies.images_poster,movies.release_data,movies.description,movies.images_bg").
		Joins("JOIN all_catagories ON movies.id = all_catagories.movie_id").
		Joins("JOIN catagories ON all_catagories.catagories_id = catagories.id").
		Where("catagories_id=?", id).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		res := NewGetCatagories{}
		err := rows.Scan(&res.ID, &res.Title, &res.Poster, &res.Date, &res.Description, &res.ImgBg)
		if err != nil {
			fmt.Println(err)
		}
		listCatagories = append(listCatagories, res)
	}
	if err != nil {
		return nil, err
	}
	return listCatagories, nil
}
