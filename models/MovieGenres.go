package models

import "github.com/jinzhu/gorm"

type MovieGenres struct {
	gorm.Model
	Gen_Type string `gorm:"column:gen_type"`
}

type MovieGenresService interface {
	CreateMovieGenres(moviegenres *MovieGenres) error
	ListGenres() ([]MovieGenres, error)
}

type moviegenresGorm struct {
	db *gorm.DB
}

func NewMovieGenresService(db *gorm.DB) MovieGenresService {
	return &moviegenresGorm{db}
}

//------------------------ Create Genres-------------------------

func (mgg *moviegenresGorm) CreateMovieGenres(moviegenres *MovieGenres) error {
	return mgg.db.Create(moviegenres).Error
}

//----------------------- List Genres -----------------------------------

func (mgg *moviegenresGorm) ListGenres() ([]MovieGenres, error) {
	genres := []MovieGenres{}
	err := mgg.db.Find(&genres).Error
	if err != nil {
		return nil, err
	}
	return genres, nil
}
