package models

import (
	"MReview-Api/hash"
	"MReview-Api/rand"
	"fmt"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

var cost = 12

type User struct {
	gorm.Model
	UserName string `gorm:"unique_index;not null" json:"username"`
	Email    string `gorm:"unique_index;not null" json:"email"`
	Password string `gorm:"not null" json:"password"`
	Token    string `gorm:"unique_index" json:"token"`
}

type UserService interface {
	Create(user *User) error
	Login(user *User) (string, error)
	GetByToken(token string) (user *User, err error)
	// GetUser(id uint) (*GetDetailUser, error)
}

type userGorm struct {
	db   *gorm.DB
	hmac *hash.HMAC
}

func NewUserService(db *gorm.DB, hmac *hash.HMAC) UserService {
	return &userGorm{db, hmac}
}

//------------------------- Create User -----------------------

func (userG *userGorm) Create(temp *User) error {
	user := new(User)
	user.UserName = temp.UserName
	user.Email = temp.Email
	user.Password = temp.Password

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), cost)
	if err != nil {
		return err
	}
	user.Password = string(hash)
	token, err := rand.GetToken()
	if err != nil {
		return err
	}

	tokenHash := userG.hmac.Hash(token)

	user.Token = tokenHash
	temp.Token = token
	return userG.db.Create(user).Error
}

//--------------------- Login User -----------------------------

func (userG *userGorm) Login(user *User) (string, error) {
	found := new(User)
	err := userG.db.Where("user_name =?", user.UserName).First(&found).Error
	if err != nil {
		return "", err
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(user.Password))
	if err != nil {
		return "", err
	}
	token, err := rand.GetToken()
	if err != nil {
		return "", err
	}
	tokenHash := userG.hmac.Hash(token)
	fmt.Println("tokenHashStr ===> ", tokenHash)

	err = userG.db.Model(&User{}).Where("id =?", found.ID).Update("token", tokenHash).Error
	if err != nil {
		return "", err
	}
	return token, nil
}

//---------------------- Get Token --------------------------------

func (userM *userGorm) GetByToken(token string) (*User, error) {

	tokenHash := userM.hmac.Hash(token)
	user := new(User)
	err := userM.db.Where("token =?", tokenHash).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil

}
