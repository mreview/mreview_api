package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type ReviewMovie struct {
	gorm.Model
	// Review_Date   time.Time `gorm:"column:review_date" sql:"datetime(6)"`
	Review_Header string `gorm:"column:review_header"`
	Review_Text   string `gorm:"column:review_text" sql:"longtext"`
	UserID        uint   `gorm:"column:user_id"`
	MovieID       uint   `gorm:"column:movie_id"`
}

type ReviewMovieService interface {
	CreateReviewMovie(reviewMovie *ReviewMovie) error
	GetReviewByMovieID(id uint) ([]ReviewMovie, error)
	GetByID(id uint) (*ReviewMovie, error)
	GetHaveUser(id uint) ([]HaveUser, error)
}

type reviewGorm struct {
	db *gorm.DB
}

func NewReviewMovieService(db *gorm.DB) ReviewMovieService {
	return &reviewGorm{db}
}

//------------------------- Create Review Movie ----------------------------

func (rg *reviewGorm) CreateReviewMovie(reviewMovie *ReviewMovie) error {
	return rg.db.Create(reviewMovie).Error
}

//------------------------- List Review Movie------------------------
func (rg *reviewGorm) GetReviewByMovieID(id uint) ([]ReviewMovie, error) {
	review := []ReviewMovie{}
	if err := rg.db.Where("movie_id=?", id).Find(&review).Error; err != nil {
		return nil, err
	}
	return review, nil
}

//-------------------------Get by Id ---------------------------

func (rg *reviewGorm) GetByID(id uint) (*ReviewMovie, error) {
	review := new(ReviewMovie)
	if err := rg.db.First(review, id).Error; err != nil {
		return nil, err
	}
	return review, nil
}

//------------------- Get Have User --------------------------------

type HaveUser struct {
	gorm.Model
	Username string `gorm:"column:user_name" json:"user_name"`
	Header   string `gorm:"column:header" json:"header"`
	Detail   string `gorm:"column:detail" json:"detail"`
	Score    int    `gorm:"column:score" json:"score"`
	RateType string `gorm:"column:rate_type" json:"rate_type"`
	// RateScore int `gorm:"column:rate_score" json:"rate_score"`
	// RateType string `gorm:"column:rate_type" json:"rate_type"`

}

func (rg *reviewGorm) GetHaveUser(id uint) ([]HaveUser, error) {
	listReview := []HaveUser{}
	rows, err := rg.db.Table("review_movies").Select("users.user_name , review_movies.review_header , review_movies.review_text ,  review_movies.created_at, rates.rate_score ,rates.rate_type").
		Joins("JOIN users ON review_movies.user_id = users.id").
		Joins("JOIN movies ON review_movies.movie_id = movies.id").
		Joins("JOIN rates ON review_movies.id =  rates.review_movie_id ").
		Where("review_movies.movie_id=?", id).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		res := HaveUser{}
		err := rows.Scan(&res.Username, &res.Header, &res.Detail, &res.CreatedAt, &res.Score, &res.RateType)
		if err != nil {
			fmt.Println(err)
		}
		listReview = append(listReview, res)
	}
	if err != nil {
		return nil, err
	}
	return listReview, nil
}
